/**
 * @author Nil Panchal Mex IT Jul 18, 2022 11:55:41 AM CourseServiceTest.java com.tarent.service
 *         TrainingAPI
 */
package com.tarent.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import com.tarent.model.Course;
import com.tarent.payload.request.CoursePayload;
import com.tarent.repository.CourseRepository;
import com.tarent.repository.CourseTransactionRepository;
import com.tarent.repository.CourseUserRepository;

/**
 * @author Nil Panchal Mex IT Jul 18, 2022 11:55:41 AM
 */
class CourseServiceTest {

  protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));



  CourseService courseService;
 
  @Mock
  Course course;
  @Mock
  private CourseRepository courseRepository;
  @Mock
  private CourseTransactionRepository courseTransactionRepository;
  @Mock
  private CourseUserRepository courseUserRepository;


  @BeforeEach
  void setUp() throws Exception {
    MockitoAnnotations.openMocks(this);
    courseService =
        new CourseService(courseRepository, courseTransactionRepository, courseUserRepository);
  }


  @Test
  void testGetCourses() {
    List<Course> courses = new ArrayList<>();
    courses.add(Course.builder().name("Java Course").description("This is java course")
        .instructorName("Jack Mex").price(500).createdAt(Instant.now()).updatedAt(Instant.now())
        .build());
    courses.add(Course.builder().name("PHP Course").description("This is php course")
        .instructorName("POP Rex").price(700).createdAt(Instant.now()).updatedAt(Instant.now())
        .build());
    when(courseRepository.findAll()).thenReturn(courses);
    List<CoursePayload> allCourses = courseService.getCourses();
    assertEquals(allCourses.size(), 2);
  }
}
