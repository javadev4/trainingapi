/**
 * @author Nil Panchal Mex IT Jul 16, 2022 10:24:19 AM Constants.java com.tarent.constant
 *         TrainingAPI
 */
package com.tarent.constant;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 10:24:19 AM
 */
public class Constants {
  public static final String UUID_GENERATOR = "UUID";
  public static final String UUID_GENERATOR_STRATEGY = "org.hibernate.id.UUIDGenerator";

  public static final String DATE_FORMAT= "yyyy-MM-dd";
  
  public static final String COURSE_ID_REQUEST_PARAM = "courseId";
  public static final String BATCH_ID_REQUEST_PARAM = "batchId";

  public static final String START_DATE_REQUEST_PARAM = "startDate";
  public static final String END_DATE_REQUEST_PARAM = "endDate";
  
  public static final String COURSE = "/course";
  public static final String SINGLE_COURSE = "/course" + "/{" + COURSE_ID_REQUEST_PARAM + "}";
  public static final String COURSE_BATCH = SINGLE_COURSE + "/batch";
  public static final String COURSE_DATE = SINGLE_COURSE + "/date";
  public static final String COURSE_USERS =
      SINGLE_COURSE + "/batch/{" + BATCH_ID_REQUEST_PARAM + "}/users";
  public static final String COURSE_SEARCH = COURSE + "/search";



}
