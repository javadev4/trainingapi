/**
 * @author Nil Panchal Mex IT Jun 26, 2022 1:10:13 PM Messages.java com.ordersystem.constant
 *         OrderSystem
 */
package com.tarent.constant;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 1:10:13 PM
 */
public class Messages {

  public static final String SUCCESS = "Success";
  public static final String FAIL = "Fail";

  public static final String COURSE_CREATED = "Course created successfully";
  public static final String COURSE_UPDATED =  "Course updated successfully";
  public static final String COURSE_BATCH_CREATED = "Course batch created successfully";
  public static final String USER_ADDED = "User added in course batch successfully";
}
