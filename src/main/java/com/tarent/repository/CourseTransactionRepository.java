/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:24:59 PM CourseTransactionRepository.java
 *         com.tarent.repository TrainingAPI
 */
package com.tarent.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tarent.model.Course;
import com.tarent.model.CourseTransaction;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:24:59 PM
 */
@Repository
public interface CourseTransactionRepository extends JpaRepository<CourseTransaction, Long> {
  List<CourseTransaction> findByStartDateGreaterThanEqualAndEndDateLessThanEqual(
      LocalDate startDate, LocalDate endDate);
  List<CourseTransaction> findByCourse(Course course);
}
