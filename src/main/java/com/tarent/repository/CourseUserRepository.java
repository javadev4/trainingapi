/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:31:44 PM CourseUserRepository.java
 *         com.tarent.repository TrainingAPI
 */
package com.tarent.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tarent.model.CourseTransaction;
import com.tarent.model.CourseUser;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:31:44 PM
 */
@Repository
public interface CourseUserRepository extends JpaRepository<CourseUser, Long> {

  List<CourseUser> findByBatch(CourseTransaction batch);
}
