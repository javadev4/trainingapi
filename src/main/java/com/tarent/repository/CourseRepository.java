/**
 * @author Nil Panchal Mex IT Jul 16, 2022 10:29:14 AM CourseRepository.java com.tarent.repository
 *         TrainingAPI
 */
package com.tarent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tarent.model.Course;

/**
 * @param <T>
 * @author Nil Panchal Mex IT Jul 16, 2022 10:29:14 AM
 */
@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

}
