/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:02:24 PM CourseService.java com.tarent.service
 *         TrainingAPI
 */
package com.tarent.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import com.tarent.constant.Constants;
import com.tarent.constant.Messages;
import com.tarent.exception.AppException;
import com.tarent.model.Course;
import com.tarent.model.CourseTransaction;
import com.tarent.model.CourseUser;
import com.tarent.payload.request.CourseBatchPayload;
import com.tarent.payload.request.CoursePayload;
import com.tarent.payload.request.CourseUserPayload;
import com.tarent.payload.response.BaseResponse;
import com.tarent.payload.response.CourseResponse;
import com.tarent.repository.CourseRepository;
import com.tarent.repository.CourseTransactionRepository;
import com.tarent.repository.CourseUserRepository;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:02:24 PM
 */
@Service
public class CourseService {
  private static final Logger log = LoggerFactory.getLogger(CourseService.class);


  private final CourseRepository courseRepository;
  private final CourseTransactionRepository courseTransactionRepository;
  private final CourseUserRepository courseUserRepository;

  public CourseService(CourseRepository courseRepository,
      CourseTransactionRepository courseTransactionRepository,
      CourseUserRepository courseUserRepository) {
    this.courseRepository = courseRepository;
    this.courseTransactionRepository = courseTransactionRepository;
    this.courseUserRepository = courseUserRepository;
  }

  public CourseResponse createCourse(CoursePayload request) {
    log.info("createCourse -> ");
    Course course = courseRepository.saveAndFlush(Course.builder().name(request.getName())
        .description(request.getDescription()).instructorName(request.getInstructorName())
        .price(request.getPrice()).createdAt(Instant.now()).updatedAt(Instant.now()).build());
    return CourseResponse.builder().code(HttpStatus.CREATED.value()).status(Messages.SUCCESS)
        .message(Messages.COURSE_CREATED).course(getCourseResponse(course)).build();
  }

  private CoursePayload getCourseResponse(Course course) {
    return CoursePayload.builder().id(course.getId()).name(course.getName())
        .description(course.getDescription()).instructorName(course.getInstructorName())
        .price(course.getPrice()).build();
  }

  // Get all courses
  public List<CoursePayload> getCourses() {
    return courseRepository.findAll().stream().map(course -> getCourseResponse(course))
        .collect(Collectors.toList());
  }


  public BaseResponse addCourseBatch(Long courseId, CourseBatchPayload request) {
    Optional<Course> course = courseRepository.findById(courseId);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
    LocalDate startDate = LocalDate.parse(request.getStartDate(), formatter);
    LocalDate endDate = LocalDate.parse(request.getEndDate(), formatter);
    courseTransactionRepository.saveAndFlush(CourseTransaction.builder().course(course.get())
        .startDate(startDate).endDate(endDate).build());
    return BaseResponse.builder().code(HttpStatus.OK.value()).status(Messages.SUCCESS)
        .message(Messages.COURSE_BATCH_CREATED).build();
  }


  // filter courses
  public Model searchCourses(String startDateString, String endDateString, Model model) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
    LocalDate startDate = LocalDate.parse(startDateString, formatter);
    LocalDate endDate = LocalDate.parse(endDateString, formatter);

    return model.addAttribute("data",
        courseTransactionRepository
            .findByStartDateGreaterThanEqualAndEndDateLessThanEqual(startDate, endDate).stream()
            .map(data -> getFilteredCourse(data)).collect(Collectors.toList()));
  }

  private CourseBatchPayload getFilteredCourse(CourseTransaction courseTransaction) {
    return CourseBatchPayload.builder().id(courseTransaction.getId())
        .name(courseTransaction.getCourse().getName())
        .description(courseTransaction.getCourse().getDescription())
        .instructorName(courseTransaction.getCourse().getInstructorName())
        .price(courseTransaction.getCourse().getPrice())
        .startDate(courseTransaction.getStartDate().toString())
        .endDate(courseTransaction.getEndDate().toString()).build();
  }

  // get course dates
  public Model getCourseDates(Long courseId, Model model) {
    Optional<Course> course = courseRepository.findById(courseId);
    return model.addAttribute("data", courseTransactionRepository.findByCourse(course.get())
        .stream().map(data -> getFilteredCourse(data)).collect(Collectors.toList()));
  }

  // update courses
  public CourseResponse updateCourse(Long courseId, CoursePayload coursePayload) {
    Course course = courseRepository.saveAndFlush(courseRepository.findById(courseId).get()
        .toBuilder().name(coursePayload.getName()).description(coursePayload.getDescription())
        .instructorName(coursePayload.getInstructorName()).price(coursePayload.getPrice()).build());
    return CourseResponse.builder().code(HttpStatus.CREATED.value()).status(Messages.SUCCESS)
        .message(Messages.COURSE_UPDATED).course(getCourseResponse(course)).build();
  }

  // book course
  public BaseResponse bookCourse(Long courseId, Long batchId, CourseUserPayload request) {
    Optional<CourseTransaction> courseTransaction = courseTransactionRepository.findById(batchId);
    List<CourseUser> courseUsers = courseUserRepository.findByBatch(courseTransaction.get());
    if (courseUsers.size() > 10) {
      throw AppException.customerOrderExistsException();
    }
    courseUserRepository.saveAndFlush(
        CourseUser.builder().firstName(request.getFirstName()).lastName(request.getLastName())
            .email(request.getEmail()).batch(courseTransaction.get()).build());
    return BaseResponse.builder().code(HttpStatus.OK.value()).status(Messages.SUCCESS)
        .message(Messages.USER_ADDED).build();
  }



}
