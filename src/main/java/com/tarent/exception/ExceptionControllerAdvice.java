/**
 * @author Nil Panchal Mex IT Jun 26, 2022 7:34:04 PM ExceptionControllerAdvice.java
 *         com.ordersystem.exception OrderSystem
 */
package com.tarent.exception;



import java.util.NoSuchElementException;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.tarent.constant.Messages;
import com.tarent.payload.response.BaseResponse;

@ControllerAdvice
public class ExceptionControllerAdvice {
  @ExceptionHandler({ConstraintViolationException.class, MethodArgumentNotValidException.class,
      HttpMessageNotReadableException.class, NoSuchElementException.class })
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ResponseEntity<BaseResponse> handleMethodArgumentNotValidException(Exception exception) {
    String errorMessage = "Invalid Request";
    if (exception instanceof HttpMessageNotReadableException) {
      errorMessage = "Invalid value";
    } else if (exception instanceof MethodArgumentNotValidException) {
      MethodArgumentNotValidException ex = (MethodArgumentNotValidException) exception;
      errorMessage = ex.getBindingResult().getFieldError().getDefaultMessage();
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(BaseResponse.builder()
        .code(HttpStatus.BAD_REQUEST.value()).status(Messages.FAIL).message(errorMessage).build());
  }


  @ExceptionHandler(AppException.class)
  public ResponseEntity<BaseResponse> handleCustomException(AppException exception) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
        .body(BaseResponse.builder().code(HttpStatus.BAD_REQUEST.value()).status(Messages.FAIL)
            .message(exception.getMessage()).build());
  }
}
