/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:01:19 PM CourseController.java com.tarent.controller
 *         TrainingAPI
 */
package com.tarent.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tarent.constant.Constants;
import com.tarent.payload.request.CoursePayload;
import com.tarent.payload.response.CourseResponse;
import com.tarent.service.CourseService;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:01:19 PM
 */
@RestController
public class CourseController {
  private static final Logger log = LoggerFactory.getLogger(CourseController.class);
  private final CourseService courseService;

  public CourseController(CourseService courseService) {
    this.courseService = courseService;
  }

  // To create course
  @PostMapping(Constants.COURSE)
  public ResponseEntity<CourseResponse> createCourse(@RequestBody CoursePayload request) {
    log.info("createCourse -> ");
    CourseResponse response = courseService.createCourse(request);
    log.info("Sending response");
    return ResponseEntity.status(response.getCode()).body(response);
  }

  // To get all courses
  @GetMapping(Constants.COURSE)
  public ResponseEntity<List<CoursePayload>> getCourses() {
    log.info("getCourses -> ");
    return ResponseEntity.status(HttpStatus.OK.value()).body(courseService.getCourses());
  }


  // To filter courses
  @GetMapping(Constants.COURSE_SEARCH)
  public ResponseEntity<Model> searchCourses(
      @RequestParam(name = Constants.START_DATE_REQUEST_PARAM, required = false) String startDate,
      @RequestParam(name = Constants.END_DATE_REQUEST_PARAM, required = false) String endDate,
      Model model) {
    return ResponseEntity.status(HttpStatus.OK.value())
        .body(courseService.searchCourses(startDate, endDate, model));
  }

  // To get course dates
  @GetMapping(Constants.COURSE_DATE)
  public ResponseEntity<Model> getCourseDates(
      @PathVariable(name = Constants.COURSE_ID_REQUEST_PARAM) Long courseId, Model model) {
    return ResponseEntity.status(HttpStatus.OK.value())
        .body(courseService.getCourseDates(courseId, model));
  }

  // To update courses
  @PatchMapping(Constants.SINGLE_COURSE)
  public ResponseEntity<CourseResponse> updateCourse(
      @PathVariable(name = Constants.COURSE_ID_REQUEST_PARAM) Long courseId,
      @RequestBody CoursePayload coursePayload) {
    CourseResponse response = courseService.updateCourse(courseId, coursePayload);
    return ResponseEntity.status(response.getCode()).body(response);
  }

}
