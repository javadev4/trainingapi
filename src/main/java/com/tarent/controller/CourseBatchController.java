/**
 * @author Nil Panchal Mex IT Jul 17, 2022 7:43:26 PM CourseBatchController.java
 *         com.tarent.controller TrainingAPI
 */
package com.tarent.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.tarent.constant.Constants;
import com.tarent.payload.request.CourseBatchPayload;
import com.tarent.payload.response.BaseResponse;
import com.tarent.service.CourseService;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 7:43:26 PM
 */
@RestController
public class CourseBatchController {
  private static final Logger log = LoggerFactory.getLogger(CourseBatchController.class);
  private final CourseService courseService;

  public CourseBatchController(CourseService courseService) {
    this.courseService = courseService;
  }

  // To add courses batch
  @PostMapping(Constants.COURSE_BATCH)
  public ResponseEntity<BaseResponse> addCourseBatch(
      @PathVariable(name = Constants.COURSE_ID_REQUEST_PARAM) Long courseId,
      @RequestBody CourseBatchPayload request) {
    log.info("addCourseBatch -> ");
    BaseResponse response = courseService.addCourseBatch(courseId, request);
    return ResponseEntity.status(response.getCode()).body(response);
  }
}
