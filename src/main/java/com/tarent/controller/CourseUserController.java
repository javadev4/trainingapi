/**
 * @author Nil Panchal Mex IT Jul 17, 2022 7:43:16 PM CourseUserController.java
 *         com.tarent.controller TrainingAPI
 */
package com.tarent.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.tarent.constant.Constants;
import com.tarent.payload.request.CourseUserPayload;
import com.tarent.payload.response.BaseResponse;
import com.tarent.service.CourseService;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 7:43:16 PM
 */
@RestController
public class CourseUserController {

  private static final Logger log = LoggerFactory.getLogger(CourseUserController.class);
  private final CourseService courseService;

  public CourseUserController(CourseService courseService) {
    this.courseService = courseService;
  }

  // To book course
  @PostMapping(Constants.COURSE_USERS)
  public ResponseEntity<BaseResponse> bookCourse(
      @PathVariable(name = Constants.COURSE_ID_REQUEST_PARAM) Long courseId,
      @PathVariable(name = Constants.BATCH_ID_REQUEST_PARAM) Long batchId,
      @RequestBody CourseUserPayload courseUserPayload) {
    log.info("bookCourse -> ");
    BaseResponse response = courseService.bookCourse(courseId, batchId, courseUserPayload);
    return ResponseEntity.status(response.getCode()).body(response);
  }
}
