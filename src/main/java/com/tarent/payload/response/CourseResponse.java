/**
 * @author Nil Panchal Mex IT Jul 16, 2022 9:56:49 PM CourseResponse.java
 *         com.tarent.payload.response TrainingAPI
 */
package com.tarent.payload.response;

import com.tarent.payload.request.CoursePayload;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 9:56:49 PM
 */
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CourseResponse extends BaseResponse {
  private CoursePayload course;
}
