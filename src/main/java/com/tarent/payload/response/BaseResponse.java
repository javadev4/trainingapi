/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:08:14 PM BaseResponse.java com.tarent.payload.response
 *         TrainingAPI
 */
package com.tarent.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 8:08:14 PM
 */
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse {
  private int code;
  private String status;
  private String message;
}
