/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:14:28 PM CourseBatchPayload.java
 *         com.tarent.payload.request TrainingAPI
 */
package com.tarent.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 12:14:28 PM
 */
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CourseBatchPayload extends CoursePayload{
  private String startDate;
  private String endDate;
}
