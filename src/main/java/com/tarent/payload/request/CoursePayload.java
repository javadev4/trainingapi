/**
 * @author Nil Panchal Mex IT Jul 16, 2022 10:19:54 AM CoursePayload.java com.tarent.payload.request
 *         TrainingAPI
 */
package com.tarent.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jul 16, 2022 10:19:54 AM
 */
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CoursePayload {
  private Long id;
  private String name;
  private String description;
  private String instructorName;
  private double price;
}
