/**
 * @author Nil Panchal Mex IT Jul 17, 2022 6:46:40 PM CourseUserPayload.java
 *         com.tarent.payload.request TrainingAPI
 */
package com.tarent.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jul 17, 2022 6:46:40 PM
 */
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CourseUserPayload {
  private String firstName;
  private String lastName;
  private String email;
}

