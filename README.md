# Training API

## Getting started

It is online training management system

Features
- Create course
- Create course batches 
  Example : 
  Batch 1 Details
  Course Name : Java Course
  Start Date 2022-01-01
  End Date : 2022-01-31
- Add users to course batch



## API to add course


```
URL : http://localhost:9555/tarent/api/course
Method : POST
```

Request Body

```
{
    "name": "Test2",
    "description": "2description",
    "instructorName": "3instructorName",
    "price": 24000
}
```



## API to add course batch

```
URL : http://localhost:9555/tarent/api/course/1/batch
Method : POST
```

Request Body

```
{
    "startDate"  : "2022-06-01",
    "endDate"  : "2022-06-30"
}
```




## API to get All Courses

```
URL : http://localhost:9555/tarent/api/course
Method : GET
```




## API to get filter courses

```
URL : http://localhost:9555/tarent/api/course/search?startDate=2022-05-01&endDate=2022-05-30
Method : GET
```




## API to get course Dates

```
URL : http://localhost:9555/tarent/api/course/1/date
Method : GET
```



## API to update course

```
URL : http://localhost:9555/tarent/api/course/1
Method : GET
```



## API to update course

```
URL : http://localhost:9555/tarent/api/course/2/batch/8/users
Method : POST
```

Request Body

```
{
    "firstName": "Nil",
    "lastName": "Panchal",
    "email": "abcd@test.com"
}
```
>>>>>>> refs/heads/master
